class Square {
    constructor(verticalPosition, horizontalPosition, oneSideSize) {
        this.verticalPosition = verticalPosition;
        this.horizontalPosition = horizontalPosition;
        this.oneSideSize = oneSideSize;
    }
}